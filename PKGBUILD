# Maintainer: Sven-Hendrik Haase <svenstaro@archlinux.org>
# Maintainer: Caleb Maclennan <caleb@alerque.com>
# Contributor: Lubomir 'Kuci' Kucera <kuci24-at-gmail-dot-com>
# Contributor: Martin Rys <martin@rys.pw>

pkgname=gitlab-runner
pkgver=17.9.1
pkgrel=1
pkgdesc="The official GitLab CI runner written in Go"
arch=('x86_64')
url='https://gitlab.com/gitlab-org/gitlab-runner'
license=('MIT')
depends=('ca-certificates' 'curl' 'git' 'glibc' 'tar')
optdepends=('inetutils: hostname command')
makedepends=('git' 'go' 'git' 'mercurial' 'gox')
install=gitlab-runner.install
replaces=('gitlab-ci-multi-runner')
backup=('etc/gitlab-runner/config.toml')
noextract=("prebuilt-alpine-arm-${pkgver}.tar.xz"
           "prebuilt-alpine-arm64-${pkgver}.tar.xz"
           "prebuilt-alpine-s390x-${pkgver}.tar.xz"
           "prebuilt-alpine-x86_64-pwsh-${pkgver}.tar.xz"
           "prebuilt-alpine-x86_64-${pkgver}.tar.xz"
           "prebuilt-ubuntu-arm-${pkgver}.tar.xz"
           "prebuilt-ubuntu-arm64-${pkgver}.tar.xz"
           "prebuilt-ubuntu-s390x-${pkgver}.tar.xz"
           "prebuilt-ubuntu-x86_64-pwsh-${pkgver}.tar.xz"
           "prebuilt-ubuntu-x86_64-${pkgver}.tar.xz")
source=("git+$url.git#tag=v${pkgver}"
        "prebuilt-alpine-arm-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-arm.tar.xz"
        "prebuilt-alpine-arm64-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-arm64.tar.xz"
        "prebuilt-alpine-s390x-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-s390x.tar.xz"
        "prebuilt-alpine-x86_64-pwsh-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-x86_64-pwsh.tar.xz"
        "prebuilt-alpine-x86_64-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-alpine-x86_64.tar.xz"
        "prebuilt-ubuntu-arm-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-arm.tar.xz"
        "prebuilt-ubuntu-arm64-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-arm64.tar.xz"
        "prebuilt-ubuntu-s390x-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-s390x.tar.xz"
        "prebuilt-ubuntu-x86_64-pwsh-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-x86_64-pwsh.tar.xz"
        "prebuilt-ubuntu-x86_64-${pkgver}.tar.xz::https://gitlab-runner-downloads.s3.amazonaws.com/v${pkgver}/helper-images/prebuilt-ubuntu-x86_64.tar.xz"
        "gitlab-runner.service"
        "gitlab-runner.sysusers"
        "gitlab-runner.tmpfiles"
        "config.toml")
sha512sums=('3e6943d2e5d03b744cff3068ee5bb9655029c2fedabdeb53f6cf4cc6e881c9cc78e3fb9131f14de5a573199381f19ae02dcf296160d03193f3f9e443ed299d01'
            'ff4bc0c8fa118748e5cd35c5246980c45f987154e844ddb80731b282228864eadccef15c372589b064c1413bd37afe142fde72db1c854e5e6a29d40ee7d3bade'
            '4d3d3f8a93e309e5116f02c32b5f88f9178331e877052c0c36210a140b0a1d398c752b01df255c330408a6ba704065e05116961e0b338909d55cb1b7279955a1'
            '30b3bfcc0d1e31a98bb99bfd49e346b6a2c8ccc187be48271c27506a41296de3cbfdceb02e1ec4527e600a6f6dbd1336632c0908b8372bfaf12b63eca693bab4'
            'dc515a473eea85de2e3f6d150a3e4f426a6797d0567fcfd4f78e2e4ab600690c6e52160e21b88e78b49e6da013923b1771c777097fc0368c7eb932d138b4444a'
            '1c2f270b40799a3aa2ea7f177881eb6bd350a8f95f0fd647c9b58e26154ac8143bef61f36bef40bb585babac5fdc21abce106cbcd3f3cfe7bf7656660773123b'
            '0cce374a7177768ea0b5ef275c795d833cb16bcccbbed34724f811d75b3e2f65c97bf432753b57b16430d5e338413fd946961da950432681d0df33cbe0dc6586'
            'ccd937ce45e4a22e3faf170e3b76663bac540ee6d47f9a4802e404063bb9098f37b4bbd99869f4435131ee46d170a823506dcf090a085aefda1748bad3ec3291'
            '56f9f75dc44db52207583c13e9985adc6afa7715ff129cf1a0a5075ebaef97ad7b36d81f3c7823db921363374c3943682de13eaa387470e1b6072d0f9778f355'
            '1cb9fa992872d93ca8fc572b52ac0ed5685d75cd4373ca1d86eb951447eec179b612c4a2443f62b1a692567d4fa9072c4c0e51f34f0d202cff120c5f502868e4'
            '4e4c29eb6e952ca2f5ece8277f31af061ad629cbd1cf27f9ed2c1f209531252e6bfdfb51f93c3909d2b23beac68b4244f986db93da710ed8c74f57ef93c4fc6b'
            'a61348bc0c817a21588bd4a82daf7f230d41816b576befbc5727e37da534de6378031a49e9f5e2da34be5d78f4927a64ee856afaa80ec7b6e7037244d9dbead0'
            '8aa7f08702e99053c696fcc2aaba83beb9e9cd6f31973d82862db9350ac46df3a095377625d31fe909677525290d2de922d7a97930ed235774cb8f0da8944d40'
            '6751d9fa0b27172d1b419c4138f5ac15cbc7c9147653a7258cf1470216142c637210bb60608c7ed0974e0e4057e5ddeae32225df1bb36e7dd1f20fec71e33cc3'
            '9718b94bd0ddb09095ffb8c1e60ca1e9649dabb1747e7fc95e58e404b2f9effdeb4cfd759f5b904443dc53a4e18c02003c38f85584713deb49f6a6d1007503de')

prepare() {
  cd gitlab-runner

  local version=$(make version | grep "Current version:" | sed -n "s/.*: \(.*\)/\1/p")
  local revision=$(make version | grep "Current revision:" | sed -n "s/.*: \(.*\)/\1/p")
  local branch=$(make version | grep "Current branch:" | sed -n "s/.*: \(.*\)/\1/p")

  sed -i "s/var VERSION.*/var VERSION = \"$version\"/" common/version.go
  sed -i "s/var REVISION.*/var REVISION = \"$revision\"/" common/version.go
  sed -i "s/var BRANCH.*/var BRANCH = \"$branch\"/" common/version.go
}

build() {
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -ldflags=-linkmode=external -mod=readonly -modcacherw"

  cd gitlab-runner
  go build -o gitlab-runner .
}

package() {
  cd gitlab-runner

  install -Dm644 "${srcdir}/config.toml" "${pkgdir}/etc/gitlab-runner/config.toml"
  install -Dm644 "${srcdir}/gitlab-runner.service" "${pkgdir}/usr/lib/systemd/system/gitlab-runner.service"
  install -Dm644 "${srcdir}/gitlab-runner.sysusers" "${pkgdir}/usr/lib/sysusers.d/gitlab-runner.conf"
  install -Dm644 "${srcdir}/gitlab-runner.tmpfiles" "${pkgdir}/usr/lib/tmpfiles.d/gitlab-runner.conf"
  install -Dm755 gitlab-runner "${pkgdir}/usr/bin/gitlab-runner"
  install -Dm755 packaging/root/usr/share/gitlab-runner/clear-docker-cache "${pkgdir}/usr/share/gitlab-runner/clear-docker-cache"

  # Move prebuilt Docker images to hard-coded canonical location
  for image in prebuilt-{alpine,ubuntu}-{arm,arm64,s390x,x86_64-pwsh,x86_64}-${pkgver}.tar.xz; do
    install -Dm644 "${srcdir}/${image}" "${pkgdir}/usr/lib/gitlab-runner/helper-images/${image}"
  done

  install -Dm644 -t "${pkgdir}/usr/share/licenses/$pkgname/" LICENSE
}
